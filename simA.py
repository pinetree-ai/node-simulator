# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

# Simulate aircraft at a very high level for mission dev & training

import asyncio
import numpy as np
from datetime import datetime

from imperix import NodeLink

from pid import PID
from dynamics import simulateStep
from conv import m2geoDeg, geoDeg2m, deg2rad, rad2deg, m2ft, radWrap
from geo import distanceBetweenGeo

# Mission variables
status = "Standby"
activeWptIdx = -1
waypoints = []
home = [0,0]

# Main function
async def main():

    global status
    global waypoints
    global activeWptIdx
    global home

    # Simulation time step
    dt = 0.1 # Simulate at 10 Hz
    
    # Zero long/lat
    lng0 = -111.898
    lat0 = 33.646

    # State vector - initial state
    x = np.array([
        0,      # x: longitudinal direction - m
        0,      # y: latitudinal directional - m
        1525,   # h: altitude (m)
        0,    # Roll (rad)
        0,   # Pitch (rad)
        deg2rad(130), # Yaw (rad) - Heading is CW, zeroed at y-axis. Yaw is CCW, zeroed at x-axis
    ])

    # State velocities
    v = 15 # Forward velocity - m/s

    xd = np.array([
        v * np.cos(x[5]),
        v * np.sin(x[5]),
        0, # SLUF
        0,
        0,
        0,
    ])

    # Mission update callback
    async def missionUpdateCallback(m):

        global status
        global waypoints
        global activeWptIdx
        global home

        if "missionStatus" in m:
            status = m["missionStatus"]

        if "mission" in m:
            
            # Active waypoint
            activeWptIdx = int(m["mission"]["activeWaypoint"])

            # Waypoint data
            waypoints = m["mission"]["waypoints"]

            # Home
            if "home" in m["mission"]:
                home = m["mission"]["home"]


    # Instantiate node link
    node = NodeLink(
        missionUpdateCallback=missionUpdateCallback
    )

    # Connect and authenticate
    await node.connect()

    # Update mission to default
    await node.updateMission(activeWptIdx,status)

    # Drone heading controller
    hdgPID = PID(1,1,1,0.1)
    rollPID = PID(2,2,0.1,1)

    # Main loop
    while True:

        t = datetime.utcnow()

        # Useful parameters in aircraft dynamics and control
        v = np.linalg.norm(xd[0:2])

        hdg = rad2deg(radWrap(np.pi/2 - x[5], s=np.pi))

        # Drone control
        if status == "Active" and (len(waypoints) > 0 and activeWptIdx >= 0) or ("latitude" in home and "longitude" in home and activeWptIdx == -2):

            # Set roll and pitch to fly to waypoints
            if activeWptIdx >= 0 and activeWptIdx < len(waypoints):
                activeWpt = waypoints[activeWptIdx]

            else:
                activeWpt = {
                    "latitude": home["latitude"],
                    "longitude": home["longitude"],
                    "mode": 'target'
                }


            if activeWpt["mode"] == 'discontinuity':
                activeWptIdx += 1
                await node.updateMission(activeWptIdx,status)

            else:

                # Get bearing from current position to target waypoints
                target = np.array([
                    geoDeg2m(activeWpt["longitude"] - lng0),
                    geoDeg2m(activeWpt["latitude"] - lat0)
                ]) if activeWptIdx >= 0 else np.array([
                    geoDeg2m(home["longitude"] - lng0),
                    geoDeg2m(home["latitude"] - lat0)
                ])

                delta = target - x[0:2]
                brg = np.arctan2(delta[1], delta[0])

                # Waypoint transition
                if activeWptIdx >= 0 and activeWpt["mode"] != 'hold' and activeWptIdx < len(waypoints)-1 and waypoints[activeWptIdx+1]["mode"] != 'discontinuity' and np.linalg.norm(delta) < 80: # Transition distance
                    activeWptIdx += 1
                    await node.updateMission(activeWptIdx,status)

                # Calculate desired roll angle
                hdgError = 0

                # Params we need for hold management
                if "params" in activeWpt and "radius" in activeWpt["params"] and int(activeWpt["params"]["radius"]) > 0:
                    r = int(activeWpt["params"]["radius"])
                else:
                    r = 100

                if activeWptIdx == -2 or activeWpt["mode"] == 'target' or (activeWpt["mode"] == "hold" and np.linalg.norm(delta) > r + 4*v):

                    # Direct to waypoint if last or right before a disc
                    if activeWptIdx <= 0 or waypoints[activeWptIdx-1]["mode"] == 'discontinuity' or activeWptIdx == len(waypoints)-1 or waypoints[activeWptIdx+1]["mode"] == 'discontinuity':

                        hdgError = radWrap(x[5] - brg)

                    # Follow track to waypoint
                    else:

                        prev = np.array([
                            geoDeg2m(waypoints[activeWptIdx-1]["longitude"] - lng0),
                            geoDeg2m(waypoints[activeWptIdx-1]["latitude"] - lat0)
                        ])

                        wptDelta = target - prev
                        trk = np.arctan2(wptDelta[1], wptDelta[0])

                        trkError = radWrap(trk - brg)
                        trkJoinError = np.clip(8 * trkError, -np.pi/4, np.pi/4)

                        targetHdg = trk - trkJoinError                    
                        hdgError = radWrap(x[5] - targetHdg)

                elif activeWpt["mode"] == "hold":

                    if "params" in activeWpt and "cw" in activeWpt["params"]:
                        cw = activeWpt["params"]["cw"]
                    else:
                        cw = False

                    distToCenter = distanceBetweenGeo([
                        activeWpt["longitude"],
                        activeWpt["latitude"]
                    ], [
                        lng0 + m2geoDeg(x[0]),
                        lat0 + m2geoDeg(x[1])
                    ])

                    holdError = np.clip(0.1 * (r - distToCenter), -np.pi/4, np.pi/4)

                    if cw:
                        targetHdg = brg + np.pi/2 + holdError

                    else:
                        targetHdg = brg - np.pi/2 - holdError

                    hdgError = radWrap(x[5] - targetHdg)


                desRoll = hdgPID.run(np.clip(hdgError, -1, 1), dt)
                desRoll = np.clip(desRoll, -0.8, 0.8)

                xd[3] = np.clip(rollPID.run(4 * (desRoll - x[3]), dt), -2, 2)

                # Pitch control for altitude
                # TODO         

        else:
            x[3] = 0
            xd[3] = 0
            x[4] = -0.04


        # Constrain velocity in heading direction
        xd[0] = v * np.cos(x[5])
        xd[1] = v * np.sin(x[5])

        # Constrain yaw rate to be a function of roll
        xd[5] = 0.03 * v * -x[3]

        # Constrain flight path angle to pitch (-alpha)
        xd[2] = v * np.tan(-x[4] - 0.04)

        # Simulate dynamics
        x,xd = simulateStep(x, xd, np.array([0,0,0,0,0,0]), dt)

        # Wrap angles
        x[3] = radWrap(x[3])
        x[4] = radWrap(x[4])
        x[5] = radWrap(x[5])

        # Telemetry update
        await node.transmitTelemetry({
            "TIMESTAMP": str(t),
            "ATT_ROLL": rad2deg(x[3]),
            "ATT_PITCH": -rad2deg(x[4]),
            "ATT_HEADING": hdg,
            "LOC_LONGITUDE": lng0 + m2geoDeg(x[0]),
            "LOC_LATITUDE": lat0 + m2geoDeg(x[1]),
            "LOC_ALTITUDE": m2ft(x[2]),
            "VEL_GROUNDSPEED": v,
            "VEL_AIRSPEED": v,
            "VEL_VERTICAL_SPEED": m2ft(xd[2]) * 60,
            "STS_BATTERY": 75,
            "STS_SIGNAL": 75
        })

        await asyncio.sleep(dt - (datetime.utcnow() - t).total_seconds())


try:
    asyncio.ensure_future(main())
    asyncio.get_event_loop().run_forever()

except KeyboardInterrupt:
    pass

finally:
    asyncio.get_event_loop().close()

