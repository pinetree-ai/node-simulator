# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

from conv import deg2rad
import numpy as np

def distanceBetweenGeo(p, q):
    # Compute distance between two geo coordinates using the Haversine formula

    phi1 = deg2rad(p[1]);
    phi2 = deg2rad(q[1]);

    dphi = phi2 - phi1;
    dlmb = deg2rad(q[0] - p[0]);

    a = np.sin(dphi/2) * np.sin(dphi/2) + np.cos(phi1) * np.cos(phi2) * np.sin(dlmb/2) * np.sin(dlmb/2);

    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1-a));

    return 6371e3 * c;