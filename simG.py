# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

# Simulate a ground-based robot at a very high level for mission dev & training

import asyncio
import numpy as np
from datetime import datetime

from imperix import NodeLink

from pid import PID
from dynamics import simulateStep
from conv import m2geoDeg, geoDeg2m, deg2rad, rad2deg, m2ft, radWrap
from geo import distanceBetweenGeo

# Mission variables
status = "Standby"
activeWptIdx = -1
waypoints = []
home = [0,0]

# Main function
async def main():

    global status
    global waypoints
    global activeWptIdx
    global home

    # Simulation time step
    dt = 0.1 # Simulate at 10 Hz
    
    # Zero long/lat
    lng0 = -111.893845
    lat0 = 33.645822

    # State vector - initial state
    x = np.array([
        0.0,      # x: longitudinal direction - m
        0.0,      # y: latitudinal directional - m
        0.0 # Yaw (rad) - Heading is CW, zeroed at y-axis. Yaw is CCW, zeroed at x-axis
    ])

    # State velocities
    v = 0.0 # Forward velocity - m/s

    xd = np.array([
        0.0,
        0.0,
        0.0
    ])

    # Mission update callback
    async def missionUpdateCallback(m):

        global status
        global waypoints
        global activeWptIdx
        global home

        if "missionStatus" in m:
            status = m["missionStatus"]

        if "mission" in m:
            
            # Active waypoint
            activeWptIdx = int(m["mission"]["activeWaypoint"])

            # Waypoint data
            waypoints = m["mission"]["waypoints"]

            # Home
            if "home" in m["mission"]:
                home = m["mission"]["home"]


    # Instantiate node link
    node = NodeLink(
        missionUpdateCallback=missionUpdateCallback
    )

    # Connect and authenticate
    await node.connect(config='nodeG.cfg')

    # Update mission to default
    await node.updateMission(activeWptIdx,status)

    # Heading controller
    hdgPID = PID(1,0.2,0.1,0.1)

    # Main loop
    while True:

        t = datetime.utcnow()

        # Useful parameters in robot dynamics and control
        v = np.linalg.norm(xd[0:2])

        hdg = rad2deg(radWrap(np.pi/2 - x[2], s=np.pi))

        xdd = np.array([0.0,0.0,0.0])

        # Robot control
        if status == "Active" and activeWptIdx != -1:

            # Return to home
            if activeWptIdx == -2 and "latitude" in home and "longitude" in home:

                activeWpt = {
                    "mode": "home"
                }

                target = np.array([
                    geoDeg2m(home["longitude"] - lng0),
                    geoDeg2m(home["latitude"] - lat0)
                ])

            # Waypoint
            elif activeWptIdx >= 0 and activeWptIdx < len(waypoints) and waypoints[activeWptIdx]["mode"] != "discontinuity":

                activeWpt = waypoints[activeWptIdx]

                target = np.array([
                    geoDeg2m(activeWpt["longitude"] - lng0),
                    geoDeg2m(activeWpt["latitude"] - lat0)
                ])

            # Should never get here!
            else:

                activeWpt = None
                target = None

                # Standby
                status = "Standby"
                activeWptIdx = -1
                await node.updateMission(activeWptIdx,status)


            # Control logic            
            if activeWpt:
                
                delta = target - x[0:2]
                brg = np.arctan2(delta[1],delta[0])
                    
                if ((activeWpt["mode"] == "home" or activeWpt["mode"] == "hover") and np.linalg.norm(delta) > 2) or activeWpt["mode"] == "target":
                    
                    # Waypoint transition
                    if activeWpt["mode"] == "target" and np.linalg.norm(delta) < 3:
                        activeWptIdx += 1
                        await node.updateMission(activeWptIdx, status)

                    # Direction control
                    xd[2] = np.clip(hdgPID.run(radWrap(brg-x[2]), dt), -2, 2)

                    # Speed control
                    if v < 2:
                        xdd[0] = np.cos(x[2])
                        xdd[1] = np.sin(x[2])


                else: # Reached home or hover?

                    # Hover mode - stay active and hold and waypoint
                    if activeWpt["mode"] == "hover" and v > 0:
                        xdd[0] = -np.cos(x[2])
                        xdd[1] = -np.sin(x[2])
                        xd[2] = 0

                    else:
                        # Standby
                        status = "Standby"
                        activeWptIdx = -1
                        await node.updateMission(activeWptIdx,status)


        elif v > 0: # Standby (slow down to a halt)
            xdd[0] = -np.cos(x[2])
            xdd[1] = -np.sin(x[2])
            xd[2] = 0

        else: # Standby - keep stationary
            xd[0] = 0
            xd[1] = 0
            xd[2] = 0


        # Constrain velocity in heading direction
        xd[0] = v * np.cos(x[2])
        xd[1] = v * np.sin(x[2])

        # Simulate dynamics
        x,xd = simulateStep(x, xd, xdd, dt)

        # Wrap angles
        x[2] = radWrap(x[2])

        # Telemetry update
        await node.transmitTelemetry({
            "TIMESTAMP": str(t),
            "ATT_ROLL": 0,
            "ATT_PITCH": 0,
            "ATT_HEADING": hdg,
            "LOC_LONGITUDE": lng0 + m2geoDeg(x[0]),
            "LOC_LATITUDE": lat0 + m2geoDeg(x[1]),
            "LOC_ALTITUDE": 0,
            "VEL_GROUNDSPEED": v,
            "VEL_AIRSPEED": v,
            "VEL_VERTICAL_SPEED": 0,
            "STS_BATTERY": 75,
            "STS_SIGNAL": 75
        })

        await asyncio.sleep(dt - (datetime.utcnow() - t).total_seconds())


try:
    asyncio.ensure_future(main())
    asyncio.get_event_loop().run_forever()

except KeyboardInterrupt:
    pass

finally:
    asyncio.get_event_loop().close()