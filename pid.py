# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

class PID:

    def __init__(self, P=1, I=0, D=0, Ilim=-1):
        self.P = P
        self.I = I
        self.D = D
        self.Ilim = Ilim
        self.error = 0
        self.integral = 0

    def run(self, error, dt):
        self.integral += dt * error
        
        if self.Ilim > 0:
            if self.integral > self.Ilim:
                self.integral = self.Ilim
            elif self.integral < -self.Ilim:
                self.integral = -self.Ilim

        P = self.P * error
        I = self.I * self.integral
        D = self.D * (error - self.error) / dt
        self.error = error
        return P + I + D