# Developed by Aptus Engineering, Inc. <https://aptus.aero>
# See LICENSE.md file in project root directory

import numpy as np

def simulateStep(x, xd, xdd, dt):

    # Euler's method - nice as simple first order models

    # Integrate acceleration to get velocity
    xd += xdd * dt

    # Integrate velocity to get new position
    x += xd * dt

    # Simple...

    return (x, xd)